#ifndef HOMEWORK2_NODE_H
#define HOMEWORK2_NODE_H

#include <ostream>

class Node {
public:
    Node(int data, Node *next, Node *prev) : data(data), next(next), prev(prev) {}

    Node(int data) : data(data) {
        next = prev = nullptr;
    }

    virtual ~Node() {
        delete next;
        delete prev;
    }

    int getData() const {
        return data;
    }

    void setData(int data) {
        Node::data = data;
    }

    Node *getNext() const {
        return next;
    }

    void setNext(Node *next) {
        Node::next = next;
    }

    Node *getPrev() const {
        return prev;
    }

    void setPrev(Node *prev) {
        Node::prev = prev;
    }

    friend std::ostream &operator<<(std::ostream &os, const Node &node) {
        os << "self: " << &node << " data: " << node.data << " prev: " << node.prev << " next: " << node.next;
        return os;
    }


private:
    int data;
    Node *next;
    Node *prev;
};

#endif //HOMEWORK2_Node_H
