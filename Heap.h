#ifndef HOMEWORK2_HEAP_H
#define HOMEWORK2_HEAP_H

#include <iostream>

class Heap {
private:
    int size;
    int capacity;
    int capacity_step;
    int *array;

    int parent(int i) {
        return i / 2;
    }

    int left(int i) {
        return 2 * i + 1;
    }

    int right(int i) {
        return 2 * i + 2;
    }

    void swapArrayContent(int *source, int *destination) {
        for (int i = 0; i < size; i++) {
            destination[i] = source[i];
        }
    }

    void increaseCapacity() {
        capacity += capacity_step;

        int *new_array = new int[capacity]();
        swapArrayContent(array, new_array);

        delete[] array;
        array = new_array;
    }

    void heapify(int i) {
        int l = left(i);
        int r = right(i);
        int largest;
        if (l < size && array[l] > array[i]) {
            largest = l;
        } else {
            largest = i;
        }

        if (r < size && array[r] > array[largest]) {
            largest = r;
        }

        if (largest != i) {
            swap(&array[i], &array[largest]);
            heapify(largest);
        }
    }

public:

    Heap() {
        size = 0;
        capacity = capacity_step = 200;
        array = new int[capacity]();
    }

    virtual ~Heap() {
    }

    int *getHeapArray() const {
        return array;
    }

    int getHeapSize() const {
        return size;
    }

    void setSize(int value) {
        this->size = value;
    }

    void decreaseSize() {
        size--;
    }

    static void swap(int *a, int *b) {
        int aux = *a;
        *a = *b;
        *b = aux;
    }

    void add(int value) {
        if (size == capacity) {
            increaseCapacity();
        }

        array[size] = value;
        size++;
    }

    void buildHeap() {
        for (int i = (size / 2); i >= 0; i--) {
            heapify(i);
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const Heap &heap) {
        os << "size: " << heap.size << ", capacity: " << heap.capacity << ", capacity_step: " << heap.capacity_step
           << ", array: [";

        for (int i = 0; i < heap.size; i++) {
            os << " " << heap.array[i];
        }

        os << " ]";

        return os;
    }
};

#endif //HOMEWORK2_HEAP_H
