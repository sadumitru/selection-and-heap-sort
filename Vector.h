#ifndef HOMEWORK2_VECTOR_H
#define HOMEWORK2_VECTOR_H

#include <iostream>
#include "Node.h"

class Vector {
private:
    Node *first;
    Node *last;
    int size;

    Node *getMax(Node *last_one) {
        Node *max = first;
        Node *temp = first;

        while (temp != last_one->getNext()) {
            if (temp->getData() > max->getData()) {
                max = temp;
            }
            temp = temp->getNext();
        }

        return max;
    }

public:

    Vector() {
        first = last = nullptr;
        size = 0;
    }

    virtual ~Vector() {
    }

    Node *getFirst() const {
        return first;
    }

    void setFirst(Node *first) {
        Vector::first = first;
    }

    Node *getLast() const {
        return last;
    }

    void setLast(Node *last) {
        Vector::last = last;
    }

    void append(int value) {
        Node *node = new Node(value);

        if (size == 0) {
            first = last = node;
        } else {
            last->setNext(node);
            node->setPrev(last);
            last = node;
        }

        size++;
    }

    void findMaxAndSwap(Node *last_one) {
        Node *max = getMax(last_one);

        if (max != last_one) {
            max->setData(max->getData() + last_one->getData());
            last_one->setData(max->getData() - last_one->getData());
            max->setData(max->getData() - last_one->getData());
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const Vector &vector) {
        os << "first: " << vector.first << ", last: " << vector.last << ", size: " << vector.size << ", nodes: [ ";

        Node *temp = vector.first;

        while (temp != nullptr) {
            os << temp->getData() << ' ';
            temp = temp->getNext();
        }

        os << "]";

        return os;
    }
};

#endif //HOMEWORK2_VECTOR_H
