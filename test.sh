#!/bin/bash

function sum_values() {
  VALUES=$1
  NR="$(($(echo "$VALUES" | tr -cd '+' | wc -c) + 1))"
  echo "scale=5; ($VALUES)/$NR" | bc -l
}

function sum_column() {
  sum_values "$(cat "$TMP"/par* | awk -v COLUMN="$1" '{print $COLUMN}' | paste -sd+)"
}

function sum() {
  printf "Vector insertion time: %20s (ns)\n" "$(sum_column 1)"
  printf "Heap   insertion time: %20s (ns)\n" "$(sum_column 2)"
  printf "Vector sort      time: %20s (ns)\n" "$(sum_column 3)"
  printf "Heap   sort      time: %20s (ns)\n" "$(sum_column 4)"
}

TMP=$(mktemp -d)
VALUES=$1
ITERATIONS=$2
ARRAY=$(seq -999999 999999 | shuf -n "$VALUES" | xargs)

parallel --bar -j"$(nproc)" --tmpdir "$TMP" --files -N0 ./cmake-build-debug/Homework2 "$ARRAY" ::: $(seq 1 "$ITERATIONS")

echo "--------------------- SUMMARY ---------------------"
echo -e "$ITERATIONS iterations for $VALUES values.\n"
sum

#rm -rf "$TMP"
