#include <iostream>
#include "Vector.h"
#include "Heap.h"
//#include <chrono>

void sort(Vector *v) {
    Node *last_one = v->getLast();

    while (last_one != v->getFirst()) {
        v->findMaxAndSwap(last_one);
        last_one = last_one->getPrev();
    }
}

void sort(Heap *h) {
    h->buildHeap();
    int size = h->getHeapSize();

    int *heap_array = h->getHeapArray();
    for (int i = size - 1; i >= 1; i--) {
        Heap::swap(&heap_array[0], &heap_array[i]);
        h->decreaseSize();
        h->buildHeap();
    }

    h->setSize(size);
}

int main(int argc, char *argv[]) {
    Vector v;
    v.append(5);
    v.append(4);
    v.append(3);
    v.append(2);
    v.append(11);
    v.append(1);
    v.append(89);
    v.append(-11);

    sort(&v);
    std::cout << v << '\n';

    Heap h;
    h.add(16);
    h.add(4);
    h.add(10);
    h.add(14);
    h.add(7);
    h.add(9);
    h.add(3);
    h.add(2);
    h.add(8);
    h.add(1);

    sort(&h);
    std::cout << h << '\n';

 /* 
    auto t1_insert_vector = std::chrono::high_resolution_clock::now();
    for (int i = 1; i < argc; i++) {
        v.append(atoi(argv[i]));
    }
    auto t2_insert_vector = std::chrono::high_resolution_clock::now();

    auto t1_insert_heap = std::chrono::high_resolution_clock::now();
    for (int i = 1; i < argc; i++) {
        h.add(atoi(argv[i]));
    }
    auto t2_insert_heap = std::chrono::high_resolution_clock::now();

    auto t1_sort_vector = std::chrono::high_resolution_clock::now();
    sort(&v);
    auto t2_sort_vector = std::chrono::high_resolution_clock::now();

    auto t1_sort_heap = std::chrono::high_resolution_clock::now();
    sort(&h);
    auto t2_sort_heap = std::chrono::high_resolution_clock::now();
 
    auto insert_duration_vector = std::chrono::duration_cast<std::chrono::nanoseconds>(
            t2_insert_vector - t1_insert_vector).count();
    auto insert_duration_heap = std::chrono::duration_cast<std::chrono::nanoseconds>(
            t2_insert_heap - t1_insert_heap).count();
    auto sort_duration_vector = std::chrono::duration_cast<std::chrono::nanoseconds>(
            t2_sort_vector - t1_sort_vector).count();
    auto sort_duration_heap = std::chrono::duration_cast<std::chrono::nanoseconds>(
            t2_sort_heap - t1_sort_heap).count(); 

    std::cout << insert_duration_vector << "\t" << insert_duration_heap << "\t" <<
              sort_duration_vector << "\t" << sort_duration_heap << '\n';
*/
    return 0;
}
